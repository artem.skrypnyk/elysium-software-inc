<?php
if (empty($_POST) || !isset($_POST)) {
    ajaxResponse('error', 'Post cannot be empty.');
} else {
    $postData = $_POST;
    $dataString = implode($postData, ",");
    $mailgun = sendMailgun($postData);
}
function ajaxResponse($status, $message, $data = NULL, $mg = NULL) {
    $response = array(
        'status' => $status,
        'message' => $message,
        'data' => $data,
        'mailgun' => $mg
    );
    $output = json_encode($response);
    exit($output);
}
function sendMailgun($data) {
    $api_key = '6495cbc88580096e366a52666111934c-8889127d-1baa3117';
    $api_domain = 'sandbox01fe3934bd5446deb179fc05e267a0b2.mailgun.org';
    $send_to = 'info@elysium-software.ca';
    $name = $data['name'];
    $email = $data['email'];
    $content = $data['message'];
    $messageBody = "Contact: $name ($email)\n\nMessage: $content";
    $config = array();
    $config['api_key'] = $api_key;
    $config['api_url'] = 'https://api.mailgun.net/v2/' . $api_domain . '/messages';
    $message = array();
    $message['from'] = $email;
    $message['to'] = $send_to;
    $message['h:Reply-To'] = $email;
    $message['subject'] = 'Mail From Elysium Software Inc. site';
    $message['text'] = $messageBody;
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $config['api_url']);
    curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($curl, CURLOPT_USERPWD, "api:{$config['api_key']}");
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $message);
    $result = curl_exec($curl);
    curl_close($curl);
    return $result;
}
?>